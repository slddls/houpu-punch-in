var token;
var reportId = [];
var piyueinfo = [];
// 登录方法
function login(tel, pwd, name) {
	// 登录参数
	let parm = {
		phone: tel,
		password: pwd,
		loginType: "android",
		uuid: ""
	}
	// 登录请求
	$.ajax({
		type: "POST",
		url: "https://api.moguding.net:9000/session/user/v1/login",
		dataType: "json",
		contentType: "application/json;charset=utf-8",
		data: JSON.stringify(parm),
		success: (msg)=> {
			if (msg.code == 200) {
				token = msg.data.token;
				// token存cookie
				var Days = 1;
				var exp = new Date();
				exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
				document.cookie = "Authorization=" + token + ";expires=会话;path=/";
				$("#loginResult").append("************" + name + "登录成功************<br/>");
				$("#ripiyue").click();
			} else {
				$("#loginResult").append("------------" + name + "登录失败------------<br/>");
			}
		}
	})
}

// 查询未批阅的报告
function weipiyue(planId, reportType) {
	let parm = { // 移动端查询参数
		"pageSize": "20",
		"state": "0", // 状态  0-未阅  4-全部   1-已阅   2-驳回
		"planId": planId, // 实习计划ID
		"reportType": reportType, // 日报：day  周报： week  月报：month
		"currPage": "1"
	};

	$.ajax({
		type: "POST",
		url: "https://api.moguding.net:9000/practice/paper/v1/listByApp",
		// url: "https://api.moguding.net:9000/practice/paper/v1/list",
		dataType: "json",
		async: false,
		contentType: "application/json;charset=utf-8",
		headers: {
			"Authorization": token
		},
		data: JSON.stringify(parm),
		success: function(msg) {
			if (msg.code != 200) {
				$("#indexInfo").append(msg.msg);
			} else {
				var data = msg.data;

				data.forEach((item) => {
					piyueinfo.push(item)
				});
				// $(".loginResult").html(name+"登录失败");
			}

		}
	})
}

// 开始批阅
function piyue(starNum, reportId, data) {
	var parm = {
		"score": "80", // 成绩 100,80,60,40,20
		"state": "1", // 状态 提交
		"starNum": starNum, // 类型 2-日报  3-周报
		"reportId": reportId
	};
	$.ajax({
		type: "POST",
		url: "https://api.moguding.net:9000/practice/paper/v1/audit", //日报
		dataType: "json",
		async: false,
		contentType: "application/json;charset=utf-8",
		headers: {
			"Authorization": token,
			"roleKey": "adviser"
		},
		data: JSON.stringify(parm),
		success: function(ru) {
			data.result = ru.msg;
		}
	})
}

// 批量批阅
function piyue2(reportType, starNum) {
	piyueinfo = [];
	planIds.forEach((item) => {
		weipiyue(item.planId, reportType);

	})
	var datainfo = $("#indexInfo").append("<ol></ol>");
	piyueinfo.forEach((item) => {
		datainfo.append("<li>" + item.reportType + "&emsp;" + item.title + "&emsp;" + item.username + "&emsp;" +
			item.teacherName + "</li>");
	});


	if (true) { // 开始批阅
		piyueinfo.forEach((item) => {
			piyue(starNum, item.reportId, item);
		})
		var datainfo = $("#indexInfo").append("<ol></ol>");
		piyueinfo.forEach((item) => {
			datainfo.append("<li>" + item.reportType + "&emsp;" + item.title + "&emsp;" + item.username + "&emsp;" +
				item.teacherName + "&emsp;" + item.result + "</li>");
		});
	}
	$("#loginResult").append("批阅完成，共批阅" + piyueinfo.length + "份报告<br/>");
}

// 补签审批
function buqian() {
	let buqianxueshen = [];
	// classIds.forEach((item, i) => {
	let parm = {
		"t": "4AA3BE931D922128A63CDB14890A330F",
		"currPage": 1,
		"pageSize": 25,
		"batchId": "80128835",
		"startTime": "",
		"endTime": "",
		"state": "APPLYINT",
		"username": "",
		"studentNumber": ""
	};
	// {
	// 	"t": "C527C34B3B8712326D9E14AC47BCF47C",
	// 	"currPage": 1,
	// 	"pageSize": 100,
	// 	"depId": "c0215553b497b18f1629bfd892249e0f",
	// 	"majorId": i < 3 ? majorId[0]:i < 6 ? majorId[1]:i < 10 ? majorId[2]:majorId[3],
	// 	"classId" : item,
	// 	"batchId" : "80128835",
	// 	"startTime": "",
	// 	"endTime": "",
	// 	"state": "APPLYINT",
	// 	"username": "",
	// 	"studentNumber": ""
	// };
	// 查询补签学生
	$.ajax({
		type: "POST",
		url: "https://api.moguding.net:9000/attendence/attendanceReplace/v1/list",
		dataType: "json",
		async: false,
		contentType: "application/json;charset=utf-8",
		headers: {
			"Authorization": token,
			"roleKey": "adviser"
		},
		data: JSON.stringify(parm),
		success: function(ru) {
			buqianxueshen = buqianxueshen.concat(ru.data)
			$("#loginResult").append(`查询出补签${ru.flag}条<br/>`);
		}
	});

	// });
	var ff = 0;
	buqianxueshen.forEach(item => {
		let parm = {
			"t": "03A87C9862AD6820035A1E24104A1976",
			"attendenceIds": [item.attendanceId],
			"comment": null,
			"applyState": 1
		};
		// 查询补签学生
		$.ajax({
			type: "POST",
			url: "https://api.moguding.net:9000/attendence/attendanceReplace/v1/audit",
			dataType: "json",
			async: false,
			contentType: "application/json;charset=utf-8",
			headers: {
				"Authorization": token
			},
			data: JSON.stringify(parm),
			success: function(ru) {
				if (ru.msg === "success") {
					ff++
					$("#indexInfo").append(`补签审核通过累计${ff}条<br/>`);
				} else {
					// $("#indexInfo").append(`${item.studentName}补签审核失败......<br/>`);
				}
			}
		});
	})
	$("#loginResult").append(`补签审核结束......共审核${ff}条<br/>`);
}
