var token = "";
var zyxm = []; //作业中的项目
var taskList = []; // 项目任务列表
var stuList = []; // 学生作业列表
var reqNum = 0; // 请求数量
var resNum = 0; //响应数
var piyueshuliang = 0;
var setIntervalid = 0;
var type=0;  // 1 上机作业   3 项目作业

 /* 加载在线jquery */
var  JSElement=document.createElement("script");		
JSElement.setAttribute("type","text/javascript");		
JSElement.setAttribute("src","//libs.baidu.com/jquery/2.1.4/jquery.min.js");		
document.body.appendChild(JSElement);

// 初始化数据
token = "";
zyxm = [];
taskList = [];
stuList = [];
reqNum = 0;
resNum = 0;
piyueshuliang = 0;
setIntervalid = 0;

/* 获取token */
var c1s = document.cookie.split(";");
var token ='';
for(let i = 0; i<c1s.length;i++){
	if(c1s[i].indexOf("blb-admin-token")>-1){
		c1 = c1s[i].split("=");
		token=c1[1];
		if(token.length>15){
			console.log(`token获取成功`);
		}
		break;
	}			
}

// 获取项目作业中的项目
function geyZYXM (){
	if(type==1){
		console.log(`开始获取上机作业`);	
	}else{
		console.log(`开始获取项目`);	
	}
	
	if(token.length>10){
		$.ajax({
			url: `https://api.bailiban.net/api/admin/homework?projectName=&createBy=&classId=&firstTagId=&secondTagId=&startTime=&endTime=&type=${type}&limit=15&page=1`,
			async: false,
			headers: {
				Authorization: token
			},
			dataFilter:function(data ){
				ress = data.replace(/\"id\":(\d+)/g,'"id": "$1"');
				ress = ress.replace(/\"createBy\":(\d+)/g,'"createBy": "$1"');
				ress = ress.replace(/\"classId\":(\d+)/g,'"classId": "$1"');
				ress = ress.replace(/\"projectId\":(\d+)/g,'"projectId": "$1"');
				ress = ress.replace(/\"thirdTagId\":(\d+)/g,'"thirdTagId": "$1"');
				return ress;
			},
			success: function(data){
				console.log(data.data.rows);
				zyxm = data.data.rows;
				if(zyxm.length>0){
					console.log(`共获取项目${zyxm.length}条`);
				}else{
					console.log(`共获取项目${zyxm.length}条，程序结束`);
					piyueshuliang = -1;
				}
			}
		});
	}else{
		alert("token有误！！！")
	}
}

// 获取项目作业中的任务
function getTaskList(createBy,classId,projectId){
	if(token.length>10){
		let url =`https://api.bailiban.net/api/admin/homework/project/work/list?createBy=${createBy}&classId=${classId}&firstTagId=&secondTagId=&projectId=${projectId}`;
		if(type==1){
			url = `https://api.bailiban.net/api/admin/homework/computer/work/list?createBy=${createBy}&classId=${classId}&thirdTagId=${projectId}`;
		}
		reqNum++;
		$.ajax({
			url: url,
			async: false,
			headers: {
				Authorization: token
			},
			dataFilter:function(data){
				ress = data.replace(/\"id\":(\d+)/g,'"homeworkId": "$1"');
				ress = ress.replace(/\"classId\":(\d+)/g,'"classId": "$1"');
				return ress;
			},
			success: function(data){
				taskList.push(...data.data);
				resNum++;
			}
		});
	}else{
		alert("token有误！！！")
	}
}

// 获取任务中的未批改学生明细
function getStuList(classId,homeworkId){
	if(token.length>10){
		reqNum++;
		$.ajax({
			url: `https://api.bailiban.net/api/admin/homework/project/work/user/info/list?`+
				`classId=${classId}&homeworkId=${homeworkId}&search=&searchType=1&limit=60&page=1`,
			async: false,
			headers: {
				Authorization: token
			},
			dataFilter:function(data){
				ress = data.replace(/\"homeworkLogId\":(\d+)/g,'"homeworkLogId": "$1"');
				return ress;
			},
			success: function(data){
				stuList.push( ...data.data.rows);
				resNum++;
			}
		});
	}else{
		alert("token有误！！！")
	}
}

// 批改学生作业
function setHomework(homeworkLogId){
	let tmp = Math.floor(Math.random()*3+2);
	let commentId = 0
	switch (tmp){
		case 2:
			commentId = 186;
			break;
		case 3:
			commentId = 117;
			break;
		case 4:
			commentId = 119;
			break;
		default:
			commentId = 116;
			tmp = 3
			break;
	}
	var parameter = {
			"commentId": commentId, // 评语标签id
			"content": "学如逆水行舟，不进则退",  //自定义评语
			"homeworkLogId": homeworkLogId, //编号
			"score": tmp  // 几星
		};
	reqNum++;
	$.ajax({
		url: `https://api.bailiban.net/api/admin/homework/homeWork/comment`,
		type: "post",
		headers: {
			"Authorization": token,
			"Content-Type": "application/json"
		},
		data: JSON.stringify(parameter),
		async: false,
		success: function(data){
			resNum++
			if(data.message==null&&data.data==null){
				piyueshuliang++;
			}
		}
	});
}
 // 间隔批阅
 function jiangepiyue(i , length) {
	if (i < length) {		
		setTimeout(function(){
			console.log(`已批改${piyueshuliang}条，批改中...`);
			setHomework(stuList[i++].homeworkLogId);
			jiangepiyue(i , length);
		}, 1000);
	} else {
		console.log(`共批改学生作业${piyueshuliang}条，程序结束`);
		return;
	}
}
// 获取任务循环
function forGetTaskList(){
	if(zyxm.length>0){
		piyueshuliang = zyxm.length; 
		zyxm.forEach(function(itme,index){
			if(type==1){
				getTaskList(itme.createBy,itme.classId,itme.thirdTagId);
			}else{
				getTaskList(itme.createBy,itme.classId,itme.projectId);
			}
			getTaskList(itme.createBy,itme.classId,itme.projectId);
			piyueshuliang--;
		});
	}else{
		console.log(`项目为空`);
	}
} 
// 获取学生明细循环
function forGetStuList(){
	if(taskList.length==0){
		console.log(`任务明细为空，程序结束`);
	}else {
		console.log(`开始获取未批改学生明细`);
		reqNum=0;
		resNum=0;
		piyueshuliang = taskList.length;
		for(let i=0; i<taskList.length;i++){
			let task=taskList[i]
			if(task.submitSum>task.correctionSum){ // 已交大于已改
				getStuList(task.classId,task.homeworkId);
				// setTimeout(function(task=taskList[i]){
				// },500);
			}
			piyueshuliang--;
		}
	}
}
// 批阅流程
function piyuekaishi(t){
	type = t;
	geyZYXM ();	
	setIntervalid = setInterval(function(){
		if(piyueshuliang==-1){ // 项目查询成功，但是没有项目
			clearInterval(setIntervalid);
		}else { // 项目查询结束
			clearInterval(setIntervalid);
			console.log(`开始获取任务明细`);
			forGetTaskList();
			setIntervalid = setInterval(function(){
				console.log(`获取中·······`);
				// 判断任务是否获取结束
				if(piyueshuliang==0 && reqNum==resNum){ // 任务获取完成 
					clearInterval(setIntervalid);
					console.log(`共获取任务${taskList.length}条`);
					if(taskList.length==0){
						console.log(`任务明细为空，程序结束`);
					}else{
						forGetStuList();
						setIntervalid = setInterval(function(){
							console.log(`获取中·······`);
							// 判断学生明细是否获取结束
							if(piyueshuliang==0 && reqNum==resNum){ // 任务获取完成 
								clearInterval(setIntervalid);
								console.log(`共获取未批改学生明细${stuList.length}条`);
								if(stuList.length==0){
									console.log(`未批改学生明细为空，程序结束`);
								}else{
									reqNum=0;
									resNum=0;
									console.log(`开始批改学生作业`);
									jiangepiyue(0, stuList.length);
								}
							}
						},500);
					}
					
				}
			},500);
		}
	},500);
}


setIntervalid = setInterval(function(){
	if($){
		clearInterval(setIntervalid)
		console.log("加载结束，请执行批阅方法")
	}else{
		console.log("加载中。请等等.....")
	}
},500)
